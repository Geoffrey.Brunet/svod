#
# Build stage
#
FROM maven:3.6.3-openjdk-17-slim AS build
WORKDIR /home/svod
COPY src /home/svod/src
COPY pom.xml /home/svod
RUN mvn -f /home/svod/pom.xml clean package -DskipTests

#
# Package stage
#
FROM openjdk:17.0-slim
COPY --from=build /home/svod/target/svod-0.0.1-SNAPSHOT.jar /usr/local/lib/svod-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/svod-0.0.1-SNAPSHOT.jar"]

